FROM openjdk:11-jdk-slim
VOLUME /tmp
COPY target/*.jar app.jar
RUN apt-get update && apt-get install dumb-init
ENTRYPOINT [ "/usr/bin/dumb-init", "--" ]
CMD ["java","-Djava.security.egd=file:/dev/./urandom","-jar","-Dspring.profiles.active=prod", "/app.jar"]