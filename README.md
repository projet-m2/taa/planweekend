# Plannification de week-ends

## Equipe M2 ILA

 - Victor HARABARI
 - Nathanaël TOUCHARD
 
## Installation de l'application

**Lancer le docker-compose :**
```bash
docker-compose up -f docker/planweekend.yml
```

L'application est disponible sur l'adresse **localhost:8080**.
 
## Installation du back en local

```bash
mvn clean install
```

```bash
mvn -B -Pprod -DskipTests package
```

```bash
docker-compose up
```

Le back-end sera accessible à l'adresse **localhost:8080**.

## Architecture

![ARchitecture en couche](images/architecture-couche.png)

## Modèle métier

![UML](images/Diagramme_UML_TAAGLI.png)

## Swagger

http://localhost:8080/swagger-ui.html

## Front-End Angular

https://gitlab.com/projet-m2/taa/planweekend-front

Nous n'avons pas eu le temps d'intégrer toutes les API, mais nous avons imaginé un aperçu de comment pourrait fonctionner l'application. Pour l'instant, seulement la création et la liste des activités fonctionnent coté front. Toute la liaison avec les localisations, les restrictions météo ne sont pas mise en place.

## Les Users Stories

Un utilisateur non connecté peut :
1. S'enregistrer
1. Se connecter
1. Utiliser la fonctionnalité "Mot de passe oublié"

Un utilisateur connecté peut :
1. Modifier ses informations personnelles
1. Modifier son mot de passe
1. Ajouter un ou une liste de sports:
	- Ajouter une nouvelle entrée
	- Ajouter des restrictions météo
	- Ajouter/Choisir une ville
1. Ajouter une ville :
	- Ajouter une nouvelle entrée
	- Ajouter/Choisir une ou une liste d'activités
1. Être notifié chaque mardi/mercredi par email.
1. Regarder/Editer les listes d'activités/localisations

## Serveur de mails
La notification de mail se fait par un serveur **maildev**

```bash
npm install -g maildev
```

Nous utilisons deux services qui permettent l'envoi de mail et la notification (service/EmailService.java et service/NotificationService.java).
