package PlanWeekEnd.Service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import PlanWeekEnd.PlanWeekEndApplication;
import PlanWeekEnd.domain.Activity;
import PlanWeekEnd.domain.Localisation;
import PlanWeekEnd.service.ActivityService;
import PlanWeekEnd.service.LocalisationService;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(classes = PlanWeekEndApplication.class)
@ActiveProfiles("dev")
public class LocalisationServiceTest {

    @Autowired
    private ActivityService activityService;

    @Autowired
    private LocalisationService localisationService;

    private Activity activity;
    private Localisation localisation;

    private Activity saveActivity;
    private Localisation saveLocalisation;

    @Transactional
    @BeforeEach
    public void setup(){
        localisation = new Localisation();
        localisation.setName("Rennes");
        localisation.setLatitude((float) -1.5959213);
        localisation.setLongitude((float) 48.8376284);

        activity = new Activity();
        activity.setType("Ski");

        localisation.addActivities(activity);
        saveActivity = activityService.createActivity(activity);
    }

    @Test
    @Order(1)
    @Transactional
    @WithMockUser(username = "user@user.com", roles = "USER")
    public void createLocalisationTest(){
        saveLocalisation = localisationService.createLocalisation(localisation);
        assertEquals(saveLocalisation, localisation);
    }

    @Test
    @Order(2)
    @Transactional
    @WithMockUser(username = "user@user.com", roles = "USER")
    public void getAllLocalisationsTest(){
        int count = localisationService.getAllLocalisations().size();
        saveLocalisation = localisationService.createLocalisation(localisation);
        assertEquals(localisationService.getAllLocalisations().size(), count + 1);
    }

    @Test
    @Order(3)
    @Transactional
    @WithMockUser(username = "user@user.com", roles = "USER")
    public void getActivitiesTest(){
        saveLocalisation = localisationService.createLocalisation(localisation);
        assertEquals(localisationService.getActivities(saveLocalisation.getId()).size(), 1);
    }

    @Test
    @Transactional
    @WithMockUser(username = "user@user.com", roles = "USER")
    public void addActivityTest(){
        saveLocalisation = localisationService.createLocalisation(localisation);
        assertEquals(localisationService.addActivityToLocalisation(saveLocalisation.getId(), saveActivity.getId()), localisation);
    }
}
