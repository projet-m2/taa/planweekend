package PlanWeekEnd.Service;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

import java.io.IOException;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.support.RestGatewaySupport;

import PlanWeekEnd.PlanWeekEndApplication;
import PlanWeekEnd.config.ApplicationConfiguration;
import PlanWeekEnd.domain.Forecast;
import PlanWeekEnd.service.DateTimeService;
import PlanWeekEnd.service.StormGlassWeatherService;

@SpringBootTest(classes = PlanWeekEndApplication.class)
@ActiveProfiles("dev")
public class StormGlassWeatherServiceTest {

    private final double lat = 17.8081;
    private final double lng = 58.7984;
    private final String API_QUERY = "lng=17.8081&start=%s&end=%s&source=sg&params=waveHeight,precipitation,cloudCover,windSpeed&lat=58.7984";

    @Autowired
    private StormGlassWeatherService sgws;

    @Autowired
    RestTemplate restTemplate;

    private MockRestServiceServer mockServer;

    @BeforeEach
    public void setUp() {
        RestGatewaySupport gateway = new RestGatewaySupport();
        gateway.setRestTemplate(restTemplate);
        mockServer = MockRestServiceServer.createServer(gateway);
    }

    @Autowired
    private ApplicationConfiguration appConf;

    @Autowired
    private DateTimeService dts;

    @Test
    public void getForecast() throws IOException {

        ClassPathResource mock_result = new ClassPathResource("MockStormGlassResponse.json");

        List<LocalDate> we = dts.getNextWeekend();
        String start = Long.toString(we.get(0).toEpochSecond(LocalTime.of(0, 0), ZoneOffset.UTC));
        String end = Long.toString(we.get(1).toEpochSecond(LocalTime.of(23, 59), ZoneOffset.UTC));

        String url = appConf.getStormGlass().getApiUrl() + "?" + String.format(API_QUERY, start, end);

        mockServer.expect(ExpectedCount.once(), requestTo(url)).andExpect(method(HttpMethod.GET))
                .andRespond(withStatus(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                        .body(IOUtils.toString(mock_result.getInputStream(), Charset.defaultCharset())));

        List<Forecast> forecast = sgws.getForecastForTheWeekEnd(lat, lng);

        mockServer.verify();

        assert (forecast != null);
        assert (forecast.size() == 2);
    }

}