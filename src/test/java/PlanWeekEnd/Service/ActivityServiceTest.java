package PlanWeekEnd.Service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import PlanWeekEnd.PlanWeekEndApplication;
import PlanWeekEnd.domain.Activity;
import PlanWeekEnd.domain.Localisation;
import PlanWeekEnd.service.ActivityService;
import PlanWeekEnd.service.LocalisationService;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(classes = PlanWeekEndApplication.class)
@ActiveProfiles("dev")
public class ActivityServiceTest {

    @Autowired
    private ActivityService activityService;

    @Autowired
    private LocalisationService localisationService;

    private Activity activity;
    private Localisation localisation;

    private Activity saveActivity;
    private Localisation saveLocalisation;

    @BeforeEach
    @Transactional
    public void setup(){
        activity = new Activity();
        activity.setType("Ski");

        localisation = new Localisation();
        localisation.setName("Rennes");
        localisation.setLatitude((float) -1.5959213);
        localisation.setLongitude((float) 48.8376284);

        activity.addLocalisations(localisation);
        saveLocalisation = localisationService.createLocalisation(localisation);
    }

    @Test
    @Order(1)
    @WithMockUser(username = "user@user.com", roles = "USER")
    public void createActivityTest(){
        saveActivity = activityService.createActivity(activity);
        assertEquals(saveActivity, activity);
    }

    @Test
    @Order(2)
    @WithMockUser(username = "user@user.com", roles = "USER")
    public void getAllActivitiesTest(){
        int count = activityService.getAllActivities().size();
        saveActivity = activityService.createActivity(activity);
        assertEquals(activityService.getAllActivities().size(), count+1);
    }

    @Test
    @Order(3)
    @Transactional
    @WithMockUser(username = "user@user.com", roles = "USER")
    public void getLocalisationsTest(){
        saveActivity = activityService.createActivity(activity);
        assertEquals(activityService.getLocalisations(saveActivity.getId()).size(), 1);
    }

    @Test
    @Transactional
    @WithMockUser(username = "user@user.com", roles = "USER")
    public void addLocalisationTest(){
        saveActivity = activityService.createActivity(activity);
        assertEquals(activityService.addLocalisationToActivity(saveActivity.getId(), saveLocalisation.getId()), activity);
    }
}
