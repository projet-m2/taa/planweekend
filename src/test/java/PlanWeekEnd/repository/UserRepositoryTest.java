package PlanWeekEnd.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import PlanWeekEnd.PlanWeekEndApplication;
import PlanWeekEnd.domain.Activity;
import PlanWeekEnd.domain.User;

@SpringBootTest(classes = PlanWeekEndApplication.class)
@ActiveProfiles("dev")
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired 
    private ActivityRepository activityRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    private User user;

    private Activity activity;

    @BeforeEach
    private void init() {
        user = new User();
        user.setEmail("to@to.ro");
        user.setPassword("$2y$10$A7misNhkk5b1bK.z2B9nPO3fm/SvZDIinGIh1VbWLh8kdQjMOgR56");

        activity = new Activity();
        activity.setType("type");
    }

    @AfterEach
    private void cleanUp(){
        if(user.getId()!=null)
            userRepository.delete(user);
    }

    @Test
    public void addUserTest() {
        long userCount = userRepository.count();

        user = userRepository.save(user);

        assertEquals(userRepository.count(), userCount + 1, "Failed to add user");
        assertNotNull(user.getId());
    }

    @Test
    public void saveWithActivityTest(){
        activity = activityRepository.save(activity);
        user.getActivities().add(activity);
        user = userRepository.save(user);

        Activity activity2 = user.getActivities().get(0);

        assertEquals(activity.getId(), activity2.getId(), "Activity id does not match");
    }

}