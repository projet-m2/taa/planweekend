package PlanWeekEnd.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import PlanWeekEnd.PlanWeekEndApplication;
import PlanWeekEnd.domain.Activity;
import PlanWeekEnd.domain.Localisation;
import PlanWeekEnd.domain.Sunny;

@SpringBootTest(classes = PlanWeekEndApplication.class)
@ActiveProfiles("dev")
public class ActivityRepoTest {

    @Autowired
    private ActivityRepository activityRepository;

    @Autowired
    private MeteoRestrictionRepository meteoRestrictionRepository;

    @Autowired
    private LocalisationRepository localisationRepository;

    private Activity activity;
    private Sunny sunny;
    private Localisation localisation;

    @BeforeEach
    private void init() {
        sunny = new Sunny();

        activity = new Activity();
        activity.setType("type");

        localisation = new Localisation();
        localisation.setLongitude(10);
        localisation.setLatitude(10);
        localisation.setName("test_localisation");
    }

    @Test
    public void saveActivityTest() {
        long count = activityRepository.count();
        activity = activityRepository.save(activity);
        assertEquals(count + 1, activityRepository.count(), "Failed to add activity");
        assertNotNull(activity.getId(), "activity id is null");
    }

    @Test
    public void saveWithMeteoRestriction() {
        sunny = meteoRestrictionRepository.save(sunny);
        activity.getRestrictions().add(sunny);
        activity = activityRepository.save(activity);
        Sunny sunny2 = (Sunny) activity.getRestrictions().get(0);
        assertEquals(sunny.getId(), sunny2.getId(), "ID does not match");

    }

    @Test
    @Transactional
    public void saveWithLocalisation() {
        activity = activityRepository.save(activity);
        localisation  = localisationRepository.save(localisation);
        activity.addLocalisations(localisation);

        Localisation extLoc = localisationRepository.findById(localisation.getId()).orElseThrow();
        Activity a = extLoc.getActivities().iterator().next();
        assertEquals(activity, a);
    }

}
