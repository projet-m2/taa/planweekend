package PlanWeekEnd.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import PlanWeekEnd.PlanWeekEndApplication;
import PlanWeekEnd.domain.User;
import PlanWeekEnd.repository.UserRepository;
import PlanWeekEnd.web.rest.models.LoginModel;

@SpringBootTest(classes = PlanWeekEndApplication.class)
// @ComponentScan(basePackages = {"PlanWeekEnd.web"})
@TestConfiguration
@WebAppConfiguration
public class UserControllerTest {

    private static final String DEFAULT_EMAIL = "john.doe@mail.com";

    @Autowired
    private UserRepository userRepository;

    // @Autowired
    // private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    // @Autowired
    // private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private WebApplicationContext applicationContext;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(applicationContext).build();
    }

    @Test
    public void createUserTest() throws Exception {
        long databaseSizeBeforeCreate = userRepository.count();

        // Create the User
        LoginModel user = new LoginModel();
        user.setEmail(DEFAULT_EMAIL);
        user.setPassword("password");

        mockMvc.perform(post("/api/register").contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(user))).andExpect(status().isCreated());

        // Validate the User in the database
        int count = 0;
        User testUser = null;
        Iterable<User> userList = userRepository.findAll();
        for (User u : userList) {
            count++;
            testUser = u;
        }
        assertThat(count == databaseSizeBeforeCreate + 1);
        assertThat(testUser.getEmail()).isEqualTo(DEFAULT_EMAIL);
    }

}
