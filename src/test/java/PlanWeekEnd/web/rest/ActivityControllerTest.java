package PlanWeekEnd.web.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import PlanWeekEnd.PlanWeekEndApplication;
import PlanWeekEnd.domain.Activity;
import PlanWeekEnd.domain.Localisation;
import PlanWeekEnd.service.ActivityService;
import PlanWeekEnd.service.LocalisationService;

@SpringBootTest(classes = PlanWeekEndApplication.class)
@ActiveProfiles("dev")
@WebAppConfiguration
@WithMockUser(username = "user@user.com", roles = "USER")
public class ActivityControllerTest {

    @Autowired
    private WebApplicationContext applicationContext;

    private MockMvc mockMvc;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private LocalisationService localisationService;

    private Activity activity;
    private Localisation localisation;

    private Activity saveActivity;
    private Localisation saveLocalisation;

    @Transactional
    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(applicationContext).build();
        activity = new Activity();
        activity.setType("Ski");

        localisation = new Localisation();
        localisation.setName("Rennes");
        localisation.setLatitude((float) -1.5959213);
        localisation.setLongitude((float) 48.8376284);

        activity.addLocalisations(localisation);
        saveLocalisation = localisationService.createLocalisation(localisation);
    }

    @Test
    public void getAllActivitiesTest() throws Exception {
        saveActivity = activityService.createActivity(activity);
        mockMvc.perform(get("/api/activities")).andExpect(status().isOk());
    }

    @Test
    public void getLocalisationsByActivityTest() throws Exception {
        saveActivity = activityService.createActivity(activity);
        mockMvc.perform(get("/api/activity/" + saveActivity.getId() + "/localisations")).andExpect(status().isOk());
    }

    @Test
    public void createActivityTest() throws Exception {
        mockMvc.perform(post("/api/activity").contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(activity))).andExpect(status().isCreated());
    }

    @Test
    public void addLocalisationToActivityTest() throws Exception {
        saveActivity = activityService.createActivity(activity);
        mockMvc.perform(put("/api/activity/" + saveActivity.getId() + "/localisation/" + saveLocalisation.getId()))
                .andExpect(status().isOk());
    }
}
