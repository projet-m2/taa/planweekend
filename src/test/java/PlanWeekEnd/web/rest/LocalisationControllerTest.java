package PlanWeekEnd.web.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import PlanWeekEnd.PlanWeekEndApplication;
import PlanWeekEnd.domain.Activity;
import PlanWeekEnd.domain.Localisation;
import PlanWeekEnd.service.ActivityService;
import PlanWeekEnd.service.LocalisationService;

@SpringBootTest(classes = PlanWeekEndApplication.class)
@ActiveProfiles("dev")
@WebAppConfiguration
public class LocalisationControllerTest {

    @Autowired
    private WebApplicationContext applicationContext;

    private MockMvc mockMvc;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private LocalisationService localisationService;

    private Activity activity;
    private Localisation localisation;

    private Activity saveActivity;
    private Localisation saveLocalisation;

    @Transactional
    @BeforeEach
    public void setup(){
        this.mockMvc = MockMvcBuilders.webAppContextSetup(applicationContext).build();

        localisation = new Localisation();
        localisation.setName("Rennes");
        localisation.setLatitude((float) -1.5959213);
        localisation.setLongitude((float) 48.8376284);

        activity = new Activity();
        activity.setType("Ski");

        localisation.addActivities(activity);
        saveActivity = activityService.createActivity(activity);
    }

    @Test
    @Transactional
    @WithMockUser(username = "user@user.com", roles = "USER")
    public void getAllActivitiesTest() throws Exception {
        saveLocalisation = localisationService.createLocalisation(localisation);
        mockMvc.perform(get("/api/localisations")).andExpect(status().isOk());
    }

    @Test
    @Transactional
    @WithMockUser(username = "user@user.com", roles = "USER")
    public void getActivityByLocalisationTest() throws Exception {
        saveLocalisation = localisationService.createLocalisation(localisation);
        mockMvc.perform(get("/api/localisation/"+saveLocalisation.getId()+"/activities")).andExpect(status().isOk());
    }

    @Test
    @Transactional
    @WithMockUser(username = "user@user.com", roles = "USER")
    public void createLocalisationTest() throws Exception {
        saveLocalisation = localisationService.createLocalisation(localisation);
        mockMvc.perform(post("/api/localisation").contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(activity))).andExpect(status().isCreated());
    }

    @Test
    @Transactional
    @WithMockUser(username = "user@user.com", roles = "USER")
    public void addActivityToLocalisationTest() throws Exception {
        saveLocalisation = localisationService.createLocalisation(localisation);
        mockMvc.perform(put("/api/localisation/" + saveLocalisation.getId() +"/activity/"+saveActivity.getId())).andExpect(status().isOk());
    }
}
