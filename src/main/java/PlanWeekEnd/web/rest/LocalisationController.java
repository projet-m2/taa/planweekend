package PlanWeekEnd.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import PlanWeekEnd.domain.Activity;
import PlanWeekEnd.domain.Localisation;
import PlanWeekEnd.service.LocalisationService;
import PlanWeekEnd.service.UserService;

@RestController
@RequestMapping("/api")
public class LocalisationController {

    @Autowired
    private LocalisationService localisationService;

    @Autowired
    private UserService userService;

    
    /**
     * {@code POST /localisations} : get the list of all localisations
     */
    @GetMapping("/localisations")
    public List<Localisation> getAllLocalisations(){
        return userService.getUserWithAuthorities().get().getLocalisations();
    }


    /**
     * {@code GET /localisation/{id}} : 
     * @param id
     */
    @GetMapping("/localisation/{id}")
    public ResponseEntity<Localisation> getLocalisation(@PathVariable Long id){
        var res =  localisationService.getLocalisationById(id).get();
        return ResponseEntity.ok(res);
    }


    @GetMapping("/localisation/{localisationId}/activities")
    public Set<Activity> getActivitiesByLocalisation(@PathVariable Long localisationId){
        return localisationService.getActivities(localisationId);
    }

    @PostMapping("/localisation")
    public ResponseEntity<Localisation> createLocalisation(@RequestBody Localisation localisation) throws URISyntaxException {
        Localisation result = localisationService.createLocalisation(localisation);
        HttpHeaders httpHeaders = new HttpHeaders();
        return ResponseEntity.created(new URI("/api/localisation/" + result.getId()))
                .headers(httpHeaders)
                .body(result);
    }

    @PutMapping("/localisation/{localisationId}/activity/{activityId}")
    public ResponseEntity<Localisation> addActivityToLocalisation(@PathVariable Long localisationId, @PathVariable Long activityId ){
        Localisation result = localisationService.addActivityToLocalisation(localisationId, activityId);
        return ResponseEntity.ok(result);
    }


}
