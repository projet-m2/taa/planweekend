package PlanWeekEnd.web.rest;

import java.net.URISyntaxException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import PlanWeekEnd.domain.User;
import PlanWeekEnd.security.jwt.JWTFilter;
import PlanWeekEnd.security.jwt.TokenProvider;
import PlanWeekEnd.service.UserService;
import PlanWeekEnd.web.rest.errors.InvalidPasswordException;
import PlanWeekEnd.web.rest.models.JWTokenModel;
import PlanWeekEnd.web.rest.models.LoginModel;

/**
 * Controller to authenticate users.
 */
@RestController
@RequestMapping("/api")
public class UserAuthenticationController {

    @Autowired
    private TokenProvider tokenProvider;

    @Autowired
    private AuthenticationManagerBuilder authenticationManagerBuilder;

    @Autowired
    private UserService userService;

    /**
     * {@code POST /authenticate} : obtain user JWT token
     * 
     * @param loginModel
     * @return JWT token used for calling API
     */
    @PostMapping("/authenticate")
    public ResponseEntity<JWTokenModel> authorize(@Valid @RequestBody LoginModel loginModel) {

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                loginModel.getEmail(), loginModel.getPassword());

        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        boolean rememberMe = (loginModel.getRememberMe() == null) ? false : loginModel.getRememberMe();
        String jwt = tokenProvider.createToken(authentication, rememberMe);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
        return new ResponseEntity<>(new JWTokenModel(jwt), httpHeaders, HttpStatus.OK);
    }

    /**
     * {@code POST  /api/register} : register the user.
     *
     * @param LoginModel  the managed user View Model.
     * @param userService
     * @throws URISyntaxException
     * @throws InvalidPasswordException  {@code 400 (Bad Request)} if the password
     *                                   is incorrect.
     * @throws EmailAlreadyUsedException {@code 400 (Bad Request)} if the email is
     *                                   already used.
     * @throws LoginAlreadyUsedException {@code 400 (Bad Request)} if the login is
     *                                   already used.
     */
    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public void registerAccount(@Valid @RequestBody LoginModel LoginModel) {
        if (!checkPasswordLength(LoginModel.getPassword())) {
            throw new InvalidPasswordException();
        }
        User user = new User();
        user.setEmail(LoginModel.getEmail());
        user.setPassword(LoginModel.getPassword());
        user = userService.registerUser(user);
    }

    private static boolean checkPasswordLength(String password) {
        return !StringUtils.isEmpty(password) && password.length() >= LoginModel.PASSWORD_MIN_LENGTH
                && password.length() <= LoginModel.PASSWORD_MAX_LENGTH;
    }
}
