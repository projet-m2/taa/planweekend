package PlanWeekEnd.web.rest.errors;

import java.net.URI;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class UserAlreadyExistsException extends AbstractThrowableProblem {

    private static final long serialVersionUID = 1L;

    public UserAlreadyExistsException(){
        super(URI.create("/error"), "user already exists", Status.BAD_REQUEST);
    }
}
