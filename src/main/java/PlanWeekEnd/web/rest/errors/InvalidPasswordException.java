package PlanWeekEnd.web.rest.errors;

import java.net.URI;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class InvalidPasswordException extends AbstractThrowableProblem {

    private static final long serialVersionUID = 1L;

    public InvalidPasswordException(){
        super(URI.create("/error"), "Incorrect password", Status.BAD_REQUEST);
    }
}
