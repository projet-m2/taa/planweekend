package PlanWeekEnd.web.rest.errors;

import java.net.URI;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class AccountResourceException extends AbstractThrowableProblem {

    private static final long serialVersionUID = 1L;

    public AccountResourceException(String reason){
        super(URI.create("/error"), reason, Status.BAD_REQUEST);
    }
}
