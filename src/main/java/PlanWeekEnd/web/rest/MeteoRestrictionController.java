package PlanWeekEnd.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import PlanWeekEnd.domain.MeteoRestriction;
import PlanWeekEnd.domain.NoRain;
import PlanWeekEnd.domain.Sunny;
import PlanWeekEnd.domain.Waves;
import PlanWeekEnd.domain.Wind;
import PlanWeekEnd.service.ActivityService;
import PlanWeekEnd.service.MeteoRestrictionService;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api")
@Slf4j
public class MeteoRestrictionController {

    @Autowired
    private MeteoRestrictionService meteoService;

    @Autowired
    ActivityService activityService;

    /**
     * {@code POST /activity/{activityId}/norain} : add NoRain meteo-restriction to an activity
     * 
     * @param activityId
     * @param mr
     * @return
     */
    @PostMapping("/activity/{activityId}/norain")
    public MeteoRestriction addMeteoRestriction(@PathVariable Long activityId, @RequestBody NoRain mr) {
        log.info("Saving NoRain for activity %d", activityId);
        mr = (NoRain) meteoService.createMeteoRestriction(mr);
        activityService.addMeteoRestriction(activityId, mr);
        return mr;
        
    }

    /**
     * {@code POST /activity/{activityId}/sunny} : add Sunny meteo-restriction to an activity
     * 
     * @param activityId
     * @param mr
     * @return
     */
    @PostMapping("/activity/{activityId}/sunny")
    public MeteoRestriction addMeteoRestriction(@PathVariable Long activityId, @RequestBody Sunny mr) {
        log.info("Saving NoRain for activity %d", activityId);
        mr = (Sunny) meteoService.createMeteoRestriction(mr);
        activityService.addMeteoRestriction(activityId, mr);
        return mr;
    }

        /**
     * {@code POST /activity/{activityId}/waves} : add Waves meteo-restriction to an activity
     * 
     * @param activityId
     * @param mr 
     * @return
     */
    @PostMapping("/activity/{activityId}/waves")
    public MeteoRestriction addMeteoRestriction(@PathVariable Long activityId, @RequestBody Waves mr) {
        log.info("Saving NoRain for activity %d", activityId);
        mr = (Waves) meteoService.createMeteoRestriction(mr);
        activityService.addMeteoRestriction(activityId, mr);
        return mr;
    }

        /**
     * {@code POST /activity/{activityId}/wind} : add Wind meteo-restriction to an activity
     * 
     * @param activityId
     * @param mr 
     * @return
     */
    @PostMapping("/activity/{activityId}/wind")
    public MeteoRestriction addMeteoRestriction(@PathVariable Long activityId, @RequestBody Wind mr) {
        log.info("Saving NoRain for activity %d", activityId);
        mr = (Wind) meteoService.createMeteoRestriction(mr);
        activityService.addMeteoRestriction(activityId, mr);
        return mr;
    }

    @DeleteMapping("/activity/{activityId}/restriction/{restrictionId}")
    public void deteleRestriction(@PathVariable Long activityId, @PathVariable Long restrictionId)
    {
        var activity = activityService.getActivityById(activityId).get();
        activity.getRestrictions().removeIf(r -> r.getId() == restrictionId);
    }
}