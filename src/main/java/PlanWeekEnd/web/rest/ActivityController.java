package PlanWeekEnd.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import PlanWeekEnd.domain.Activity;
import PlanWeekEnd.domain.Localisation;
import PlanWeekEnd.service.ActivityService;
import PlanWeekEnd.service.UserService;

@RestController
@RequestMapping("/api")
public class ActivityController {

    @Autowired
    private ActivityService activityService;

    @Autowired
    private UserService userService;

    @GetMapping("/activities")
    public List<Activity> getAllActivities() {
        List<Activity> activities = userService.getUserWithAuthorities().get().getActivities();
        activities.size();
        return activities;
    }

    @GetMapping("/activity/{id}")
    public ResponseEntity<Activity> getAllActivities(@PathVariable Long id) {
        var result = activityService.getActivityById(id).get();
        return ResponseEntity.ok(result);
    }

    @GetMapping("/activity/{activityId}/localisations")
    public Set<Localisation> getLocalisationsByActivity(@PathVariable Long activityId) {
        return activityService.getLocalisations(activityId);
    }

    @PostMapping("/activity")
    public ResponseEntity<Activity> createActivity(@RequestBody Activity activity) throws URISyntaxException {
        Activity result = activityService.createActivity(activity);
        HttpHeaders httpHeaders = new HttpHeaders();
        return ResponseEntity.created(new URI("/api/activity/" + result.getId())).headers(httpHeaders).body(result);
    }

    @PutMapping("/activity/{activityId}/localisation/{localisationId}")
    public ResponseEntity<Activity> addLocalisationToActivity(@PathVariable Long activityId,
            @PathVariable Long localisationId) {
        Activity result = activityService.addLocalisationToActivity(activityId, localisationId);
        return ResponseEntity.ok(result);
    }

    // @PutMapping("/activity/{activityId}/restriction/")
    // public ResponseEntity<Activity> addMeteoRestrictionToActivity(@PathVariable Long activityId,
    //         @PathVariable Long restrictionId) {
    //     Activity result = activityService.addMeteoRestriction(activityId, restrictionId);
    //     return ResponseEntity.ok(result);
    // }

}
