package PlanWeekEnd.web.rest.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * Object to return as body in JWT Authentication.
 */
@Data
public class JWTokenModel {

    private String idToken;

    public JWTokenModel(String idToken) {
        this.idToken = idToken;
    }

    @JsonProperty("id_token")
    public String getIdToken() {
        return idToken;
    }
}