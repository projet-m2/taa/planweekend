package PlanWeekEnd.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import PlanWeekEnd.service.UserService;
import PlanWeekEnd.service.dto.UserDTO;
import PlanWeekEnd.web.rest.errors.AccountResourceException;

@RestController
@RequestMapping("/api")
public class AccountController {

    @Autowired
    private UserService userService;

    @GetMapping("/account")
    public UserDTO getAccount() {
        return userService.getUserWithAuthorities().map(UserDTO::new)
                .orElseThrow(() -> new AccountResourceException("User could not be found"));
    }
}