package PlanWeekEnd.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import PlanWeekEnd.security.AuthoritiesConstants;
import PlanWeekEnd.service.NotificationService;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/dev")
@Slf4j
@Secured({AuthoritiesConstants.ADMIN})
public class DevController {

    @Autowired
    private NotificationService notificationService;

    @GetMapping("/notif")
    @ResponseBody
    public String forceNotifications(){
        notificationService.sendNotifications();
        log.info("SENDING NOTIFICATIONS");
        return "NOTIFICATIONS ARE OUT";
    }
    
}