package PlanWeekEnd.domain;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * A Localisation.
 */
@Entity
@Table(name = "localisation")
@EqualsAndHashCode(doNotUseGetters = true)
@Data
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Localisation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @NotNull
    private Long id;

    private String name;

    private float longitude;
    private float latitude;

    @ManyToMany(mappedBy = "localisations")
//    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnoreProperties("localisations")
    private Set<Activity> activities = new HashSet<>();

    public Localisation addActivities(Activity activity) {
        this.activities.add(activity);
        activity.getLocalisations().add(this);
        return this;
    }

    public Localisation removeActivities(Activity activity) {
        this.activities.remove(activity);
        activity.getLocalisations().remove(this);
        return this;
    }

}
