package PlanWeekEnd.domain;
import java.io.Serializable;

import javax.persistence.Entity;

import PlanWeekEnd.service.MeteoRestrictionVisitor;
import lombok.Data;

/**
 * A Sunny.
 */
@Entity
@Data
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Sunny extends MeteoRestriction implements Serializable {

    private static final long serialVersionUID = 1L;

    protected final String type = "Sunny";
    
    @Override
    public boolean accept(MeteoRestrictionVisitor mrv){
        return mrv.visit(this);
    }
}
