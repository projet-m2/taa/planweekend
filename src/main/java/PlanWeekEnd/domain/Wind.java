package PlanWeekEnd.domain;
import java.io.Serializable;

import javax.persistence.Entity;

import PlanWeekEnd.service.MeteoRestrictionVisitor;
import lombok.Data;

/**
 * A Wind.
 */
@Entity
@Data
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Wind extends MeteoRestriction implements Serializable {

    private static final long serialVersionUID = 1L;

    protected final String type = "Wind";

    private Float Min;
    private Float Max;

    @Override
    public boolean accept(MeteoRestrictionVisitor mrv){
        return mrv.visit(this);
    }
}
