package PlanWeekEnd.domain;
import java.io.Serializable;

import javax.persistence.Entity;

import PlanWeekEnd.service.MeteoRestrictionVisitor;
import lombok.Data;

/**
 * A Rain.
 */
@Entity
@Data
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class NoRain extends MeteoRestriction implements Serializable {

    private static final long serialVersionUID = 1L;

    protected final String type = "NoRain";
    
    @Override
    public boolean accept(MeteoRestrictionVisitor mrv){
        return mrv.visit(this);
    }
}
