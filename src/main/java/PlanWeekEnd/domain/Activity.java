package PlanWeekEnd.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * A Activity.
 */
@Entity
@EqualsAndHashCode(doNotUseGetters = true)
@Data
// @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Activity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @NotNull
    private Long id;

    @Column(name = "type")
    private String type;

    @OneToMany(fetch = FetchType.EAGER)
    // @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<MeteoRestriction> restrictions = new ArrayList<>();

    @ManyToMany
    // @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "activity_localisation",
    joinColumns = @JoinColumn(name = "activity_id", referencedColumnName = "id"),
    inverseJoinColumns = @JoinColumn(name = "localisations_id", referencedColumnName = "id"))
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnoreProperties("activities")
    private Set<Localisation> localisations = new HashSet<>();

    public Activity addLocalisations(Localisation localisation) {
        this.localisations.add(localisation);
        localisation.getActivities().add(this);
        return this;
    }

    public Activity removeLocalisations(Localisation localisation) {
        this.localisations.remove(localisation);
        localisation.getActivities().remove(this);
        return this;
    }
}
