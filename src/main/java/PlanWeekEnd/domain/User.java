package PlanWeekEnd.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.hibernate.annotations.BatchSize;

import lombok.Data;

@Entity
@Data
@Table(name = "user")
public class User {
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; 

    @NotNull
    @Email    
    @Size(min = 1, max = 254)
    @Column(length = 254, unique = true, nullable = false, name = "email")
    private String email;

    @JsonIgnore
    @NotNull
    @Size(min = 60, max = 60)
    @Column(name = "password_hash", length = 60, nullable = false)
    private String password;

    // @Size(max = 50)
    // @Column(name = "first_name", length = 50)
    // private String firstName;

    // @Size(max = 50)
    // @Column(name = "last_name", length = 50)
    // private String lastName;

    // @Email
    // @Size(min = 5, max = 254)
    // @Column(length = 254, unique = true)
    // private String email;


    // @Size(min = 2, max = 10)
    // @Column(name = "lang_key", length = 10)
    // private String langKey;

    // @Size(max = 20)
    // @Column(name = "reset_key", length = 20)
    // @JsonIgnore
    // private String resetKey;

    // @Column(name = "reset_date")
    // private Instant resetDate = null;

    @JsonIgnore
    @ManyToMany
    @JoinTable(
        name = "user_authority",
        joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "authority_name", referencedColumnName = "name")})
    // @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @BatchSize(size = 20)
    private Set<Authority> authorities = new HashSet<>();

    @OneToMany
    private List<Activity> activities = new ArrayList<>();

    @OneToMany
    private List<Localisation> localisations = new ArrayList<>();
}