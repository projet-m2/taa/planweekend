package PlanWeekEnd.domain;

import java.time.LocalDate;

import lombok.Data;

/**
 * Represents a forecast object
 */
@Data
public class Forecast {

    LocalDate date;

    private float windSpeed;
    private float waveHeight;

    private boolean rain;
    private boolean sunny;

}