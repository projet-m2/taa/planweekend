package PlanWeekEnd.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import PlanWeekEnd.service.MeteoRestrictionVisitor;
import lombok.Data;

/**
 * A MeteoRestriction.
 */
@Entity
@Data
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public abstract class MeteoRestriction implements Serializable {

    private static final long serialVersionUID = 1L;

    protected final String type = "";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @NotNull
    protected Long id;

    public abstract boolean accept(MeteoRestrictionVisitor mrv);
}
