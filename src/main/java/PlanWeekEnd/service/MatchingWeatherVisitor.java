package PlanWeekEnd.service;

import PlanWeekEnd.domain.Forecast;
import PlanWeekEnd.domain.NoRain;
import PlanWeekEnd.domain.Sunny;
import PlanWeekEnd.domain.Waves;
import PlanWeekEnd.domain.Wind;

public class MatchingWeatherVisitor implements MeteoRestrictionVisitor {

    private Forecast forecast;

    public MatchingWeatherVisitor(Forecast forecast) {
        this.forecast = forecast;
    }

    @Override
    public boolean visit(NoRain r) {
        return !forecast.isRain();
    }

    @Override
    public boolean visit(Sunny r) {
        return forecast.isSunny();
    }

    @Override
    public boolean visit(Waves w) {
        var wh = forecast.getWaveHeight();
        return wh < w.getMax() && wh > w.getMin();
    }

    @Override
    public boolean visit(Wind w) {
        var ws = forecast.getWindSpeed();
        return ws < w.getMax() && ws > w.getMin();
    }

}