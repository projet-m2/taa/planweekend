package PlanWeekEnd.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import PlanWeekEnd.domain.Activity;
import PlanWeekEnd.domain.Localisation;
import PlanWeekEnd.repository.ActivityRepository;
import PlanWeekEnd.repository.LocalisationRepository;

@Service
@Transactional
public class LocalisationService {

    @Autowired
    private ActivityRepository activityRepository;

    @Autowired
    private LocalisationRepository localisationRepository;

    @Autowired
    private UserService userService;


    public Localisation createLocalisation(Localisation localisation){
        localisation = localisationRepository.save(localisation);
        var user = userService.getUserWithAuthorities().get();
        user.getLocalisations().add(localisation);
        userService.saveUser(user);
        
        return localisation;
    }

    public List<Localisation> getAllLocalisations() {
        return localisationRepository.findAll();
    }

    public Set<Activity> getActivities(Long localisationId){
        return localisationRepository.findById(localisationId).get().getActivities();
    }


    @Transactional
    public Localisation addActivityToLocalisation(Long localisationId, Long activityId) {
        Localisation localisation = localisationRepository.findById(localisationId).get();
        Activity activity = activityRepository.findById(activityId).get();
        localisation.addActivities(activity);
        return localisationRepository.save(localisation);
    }

	public Optional<Localisation> getLocalisationById(Long id) {
		return localisationRepository.findWithActivitiesById(id);
	}


}
