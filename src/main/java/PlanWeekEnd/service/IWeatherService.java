package PlanWeekEnd.service;

import java.util.List;

import org.springframework.stereotype.Service;

import PlanWeekEnd.domain.Forecast;

@Service
public interface IWeatherService {
    List<Forecast> getForecastForTheWeekEnd(double longitude, double latitude);
}