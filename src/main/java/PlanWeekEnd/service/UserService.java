package PlanWeekEnd.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import PlanWeekEnd.domain.Authority;
import PlanWeekEnd.domain.User;
import PlanWeekEnd.repository.AuthorityRepository;
import PlanWeekEnd.repository.UserRepository;
import PlanWeekEnd.security.AuthoritiesConstants;
import PlanWeekEnd.security.SecurityUtils;
import PlanWeekEnd.web.rest.errors.UserAlreadyExistsException;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired SecurityUtils securityUtils;

    public User registerUser(User user) {

        if(userRepository.findOneWithAuthoritiesByEmail(user.getEmail()).isPresent()){
            throw new UserAlreadyExistsException();
        }        

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        Set<Authority> authorities = user.getAuthorities();
        if (authorities.size() == 0) {
            authorities.add(authorityRepository.findById(AuthoritiesConstants.USER).get());
        }

        user = userRepository.save(user);

        return user;
    }
    
    public User saveUser(User user){
        return userRepository.save(user);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthorities() {
        return SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneWithAuthoritiesByEmail);
    }

    public List<String> getAuthorities() {
        return authorityRepository.findAll().stream().map(Authority::getName).collect(Collectors.toList());
    }

    public Iterable<User> getAllUsers(){
        return userRepository.findAll();
    }


}
