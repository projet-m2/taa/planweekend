package PlanWeekEnd.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import PlanWeekEnd.domain.Activity;
import PlanWeekEnd.domain.Forecast;
import PlanWeekEnd.domain.Localisation;
import PlanWeekEnd.domain.MeteoRestriction;
import PlanWeekEnd.domain.NoRain;
import PlanWeekEnd.repository.ActivityRepository;
import PlanWeekEnd.repository.LocalisationRepository;
import PlanWeekEnd.repository.MeteoRestrictionRepository;
import PlanWeekEnd.security.SecurityUtils;

@Service
@Transactional
public class ActivityService {
    @Autowired
    private ActivityRepository activityRepository;

    @Autowired
    private LocalisationRepository localisationRepository;

    @Autowired
    private MeteoRestrictionRepository meteoRestrictionRepository;

    @Autowired
    private UserService userService;

    public Activity createActivity(Activity activity){
        activity =  activityRepository.save(activity);
        
        var user = userService.getUserWithAuthorities().get();
        user.getActivities().add(activity);
        userService.saveUser(user);

        return activity;
    }

    @Transactional
    public Optional<Activity> getActivityById(long id){
        return activityRepository.findById(id);
    }

    public List<Activity> getAllActivities(){
        return activityRepository.findAll();
    }

    public Set<Localisation> getLocalisations(Long activityId){
        return activityRepository.findById(activityId).get().getLocalisations();
    }

    @Transactional
    public Activity addLocalisationToActivity(Long activityId, Long localisationId){
        Activity activity = activityRepository.findById(activityId).get();
        Localisation localisation = localisationRepository.findById(localisationId).get();
        activity.addLocalisations(localisation);
        return activityRepository.save(activity);
    }
    

    public Activity addMeteoRestriction(Long activityId, MeteoRestriction mr){
        Activity activity = activityRepository.findById(activityId).get();
        activity.getRestrictions().add(mr);
        return activityRepository.save(activity);
    }

    public boolean checkWeather(Activity a, Forecast f){
        
        return false;
    }
}
