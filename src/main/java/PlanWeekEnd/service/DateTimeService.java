package PlanWeekEnd.service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class DateTimeService {
    public List<LocalDate> getNextWeekend() {
        List<LocalDate> result = new ArrayList<>();

        LocalDate today = LocalDate.now();
        DayOfWeek dow = today.getDayOfWeek();
        LocalDate saturday = today.plusDays(-dow.compareTo(DayOfWeek.SATURDAY));
        LocalDate sunday = saturday.plusDays(1);

        result.add(saturday);
        result.add(sunday);

        return result;
    }

}