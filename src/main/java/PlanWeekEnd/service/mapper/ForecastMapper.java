package PlanWeekEnd.service.mapper;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import PlanWeekEnd.domain.Forecast;
import PlanWeekEnd.service.DateTimeService;
import PlanWeekEnd.service.dto.StormGlassForecastDTO;
import PlanWeekEnd.service.dto.StormGlassForecastDTO.HourlyForecast;

// TODO: add caching because there is a quota limit

@Service
public class ForecastMapper {

    @Autowired
    private DateTimeService dts;

    public static final LocalTime dayStart = LocalTime.of(8, 00);
    public static final LocalTime dayEnd = LocalTime.of(20, 00);
    public static final String defaultSource = "sg";

    public List<Forecast> StormGlassForecastDTOToForecast(StormGlassForecastDTO dto) {

        ArrayList<Forecast> res = new ArrayList<>(2);

        List<LocalDate> we = dts.getNextWeekend();

        // small optimisation, saving our progress makes it possible
        // to loop without parsing the whole list twice
        int position = 0;

        for (LocalDate day : we) {
            LocalDateTime start = day.atTime(dayStart);
            LocalDateTime end = day.atTime(dayEnd);

            float windSpeed = 0;
            float waveHeight = 0;
            float cloudcover = 0;
            float precipitation = 0;

            int count = 0;

            for (int i = position; i < dto.getHours().size(); i++) {
                HourlyForecast hourlyForecast = dto.getHours().get(i);
                if (hourlyForecast.getTime().toLocalDateTime().isBefore(start))
                    continue;
                if (hourlyForecast.getTime().toLocalDateTime().isAfter(end)) {
                    position = i;
                    break;
                }

                windSpeed = hourlyForecast.getWindSpeed().get(0).getValue();
                waveHeight = hourlyForecast.getWaveHeight().get(0).getValue();
                cloudcover = hourlyForecast.getCloudCover().get(0).getValue();
                precipitation = hourlyForecast.getPrecipitation().get(0).getValue();
                count++;
            }

            Forecast f = new Forecast();
            f.setDate(day);
            f.setRain(precipitation == 0);
            f.setSunny(cloudcover / count < 50);
            f.setWaveHeight(waveHeight / count);
            f.setWindSpeed(windSpeed / count);
            
            res.add(f);

        }

        return res;
    }

}