package PlanWeekEnd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import PlanWeekEnd.domain.User;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${application.email.sender}")
    private String sender;

    // stub for sending emails
    // TODO implement email service
    public void sendEmail(User user, String text) {
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(user.getEmail());
        msg.setFrom(sender);
        msg.setSubject("PlanWeekEnd");
        msg.setText(text);
        javaMailSender.send(msg);
    }
}