package PlanWeekEnd.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import PlanWeekEnd.config.ApplicationConfiguration;
import PlanWeekEnd.domain.Forecast;
import PlanWeekEnd.service.dto.StormGlassForecastDTO;
import PlanWeekEnd.service.mapper.ForecastMapper;
import lombok.extern.slf4j.Slf4j;

@Service(value = "weatherService")
@Slf4j
public class StormGlassWeatherService implements IWeatherService {

    @Autowired
    private ApplicationConfiguration appConf;

    @Autowired
    private DateTimeService dts;

    @Autowired
    private ForecastMapper forecastMapper;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public List<Forecast> getForecastForTheWeekEnd(double longitude, double latitude) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("authorization", appConf.getStormGlass().getApiKey());
        // headers.a

        Map<String, String> params = new HashMap<>();
        params.put("params", "waveHeight,precipitation,cloudCover,windSpeed");
        params.put("lng", Double.toString(longitude));
        params.put("lat", Double.toString(latitude));
        params.put("source", "sg");

        List<LocalDate> we = dts.getNextWeekend();

        params.put("start", Long.toString(we.get(0).toEpochSecond(LocalTime.of(0, 0), ZoneOffset.UTC)));
        params.put("end", Long.toString(we.get(1).toEpochSecond(LocalTime.of(23, 59), ZoneOffset.UTC)));

        String query = params.entrySet().stream().map(p -> p.getKey() + "=" + p.getValue())
                .reduce((a, b) -> a + "&" + b).orElse("");

        HttpEntity request = new HttpEntity<>(headers);
        log.error("" + request.getHeaders().size());

        String url = appConf.getStormGlass().getApiUrl() + "?" + query;
        // String url = "http://localhost:9000/result" + "?" + query;
        ResponseEntity<StormGlassForecastDTO> forecastDTO = null;
        try {
            forecastDTO = restTemplate.exchange(url, HttpMethod.GET, request, StormGlassForecastDTO.class);
        } catch (HttpClientErrorException e) {
            log.error(e.getResponseBodyAsString());
            throw e;
        }
        return forecastMapper.StormGlassForecastDTOToForecast(forecastDTO.getBody());
    }

}