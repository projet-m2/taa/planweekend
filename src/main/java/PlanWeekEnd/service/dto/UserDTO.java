package PlanWeekEnd.service.dto;

import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import PlanWeekEnd.domain.Authority;
import PlanWeekEnd.domain.User;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A DTO representing a user, with his authorities.
 */
@Data
@NoArgsConstructor
public class UserDTO {

    private Long id;

    @NotBlank
    @Email
    @Size(min = 1, max = 254)
    private String email;

    private Set<String> authorities;

    public UserDTO(User user) {
        this.id = user.getId();
        this.email = user.getEmail();
        this.authorities = user.getAuthorities().stream()
            .map(Authority::getName)
            .collect(Collectors.toSet());
    }
}
