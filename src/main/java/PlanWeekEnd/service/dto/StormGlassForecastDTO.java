package PlanWeekEnd.service.dto;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class StormGlassForecastDTO {

    /**
     * Hourly forecast data
     */
    private List<HourlyForecast> hours;

    /**
     * Query Meta information
     */
    private Meta meta;

    @Data
    public static class HourlyForecast {
        private List<simpleEntry> cloudCover;
        private List<simpleEntry> precipitation;
        private List<simpleEntry> waveHeight;
        private List<simpleEntry> windSpeed;
        private OffsetDateTime time;
    }

    @Data
    public static class simpleEntry{
        private String source;
        private float value;
    }

    @Data
    public static class Meta{
        private int cost;
        private int dailyQuota;
        private int requestCount;
        @JsonFormat(pattern =  "yyyy-MM-dd HH:mm")
        private LocalDateTime end;
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
        private LocalDateTime start;
        private double lng;
        private double lat;
        private List<String> params;
        private String source;
    }
}