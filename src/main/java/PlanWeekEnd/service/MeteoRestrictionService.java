package PlanWeekEnd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import PlanWeekEnd.domain.MeteoRestriction;
import PlanWeekEnd.repository.MeteoRestrictionRepository;

@Service
public class MeteoRestrictionService {
    
    @Autowired
    private MeteoRestrictionRepository repo;

    public MeteoRestriction createMeteoRestriction(MeteoRestriction mr){
        return repo.save(mr);
    }


}