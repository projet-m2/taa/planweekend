package PlanWeekEnd.service;

import PlanWeekEnd.domain.NoRain;
import PlanWeekEnd.domain.Sunny;
import PlanWeekEnd.domain.Waves;
import PlanWeekEnd.domain.Wind;

public interface MeteoRestrictionVisitor {
    boolean visit(NoRain r);
    boolean visit(Sunny r);
    boolean visit(Waves w);
    boolean visit(Wind w);
}