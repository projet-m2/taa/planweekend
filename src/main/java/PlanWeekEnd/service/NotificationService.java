package PlanWeekEnd.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import PlanWeekEnd.domain.Activity;
import PlanWeekEnd.domain.Localisation;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class NotificationService {

    @Autowired
    private UserService userService;

    @Autowired
    private IWeatherService weatherService;

    @Autowired
    private EmailService emailService;

    @Scheduled(cron = "${application.notifications.schedule}")
    public void sendNotifications() {
        log.info("Sending email notificaitons");
        int emailCounter = 0;
        var users = userService.getAllUsers();
        for (var user : users) {
            var activities = user.getActivities();
            if (activities.size() == 0)
                continue;

            Map<Activity, List<Pair<Localisation, LocalDate>>> events = new HashMap<>();

            for (var activity : activities) {
                var pairs = new ArrayList<Pair<Localisation, LocalDate>>();
                events.put(activity, pairs);
                for (var location : activity.getLocalisations()) {
                    var forecasts = weatherService.getForecastForTheWeekEnd(location.getLongitude(),
                            location.getLatitude());
                    for (var forecast : forecasts) {
                        var visitor = new MatchingWeatherVisitor(forecast);
                        var meteoRestrictions = activity.getRestrictions();
                        var matches = meteoRestrictions.stream().map(r -> r.accept(visitor)).reduce((a, b) -> a && b);
                        if (meteoRestrictions.size() == 0 || matches.orElse(false)) {
                            pairs.add(Pair.of(location, forecast.getDate()));
                        }
                    }
                }
            }

            var dateFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);
            var sb = new StringBuilder();
            sb.append(String.format("Dear %s\n", user.getEmail()));
            if (events.size() > 0) {
                sb.append("You have following activities available this weekend:\n\n");
                for (var entry : events.entrySet()) {
                    if (entry.getValue().size() == 0)
                        continue;

                    sb.append(String.format("    %s:\n", entry.getKey().getType()));
                    for (var pair : entry.getValue()) {
                        sb.append(String.format("        - at %s, %s\n", pair.getFirst().getName(),
                                pair.getSecond().format(dateFormatter)));
                    }
                }
            }else{
                sb.append("There are no activities available for you this weekend");
            }
            emailCounter++;
            this.emailService.sendEmail(user, sb.toString());

        }
        log.info("{} notifications were sent", emailCounter);
    }
}