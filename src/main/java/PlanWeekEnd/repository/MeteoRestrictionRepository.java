package PlanWeekEnd.repository;

import org.springframework.data.repository.CrudRepository;

import PlanWeekEnd.domain.MeteoRestriction;

public interface MeteoRestrictionRepository extends CrudRepository<MeteoRestriction, Long> {
    
}