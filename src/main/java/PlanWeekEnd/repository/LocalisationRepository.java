package PlanWeekEnd.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import PlanWeekEnd.domain.Localisation;

public interface LocalisationRepository extends CrudRepository<Localisation, Long> {

    List<Localisation> findAll();
    
	Optional<Localisation> findWithActivitiesById(Long id);
}
