package PlanWeekEnd.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import PlanWeekEnd.domain.Activity;

public interface ActivityRepository extends CrudRepository<Activity, Long>{

    List<Activity> findAll();
    List<Activity> findOneWithMeteoRestrictionsById(long id);
}
