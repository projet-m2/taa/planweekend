package PlanWeekEnd.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;

import lombok.Data;

/**
 * Application configuration class Reflects configuration from application.yml
 * files
 */
@Configuration
@ConfigurationProperties(prefix = "application")
@Data
public class ApplicationConfiguration {
    /**
     * Application security configuration
     */
    private ApplicationSecurity security;
    /**
     * OpenWeatherMap api configuration
     */
    private OpenWeatherConfiguration openWeatherMap;
    /**
     * Cross-origin request security configuration
     */
    private CorsConfiguration cors;
    /**
     * StormGlass api configuration
     */
    private StormGlassConfiguration stormGlass;

    @Data
    static public class OpenWeatherConfiguration {
        private String key;
    }

    @Data
    static public class StormGlassConfiguration {
        private String apiUrl;
        private String apiKey;
    }

}