package PlanWeekEnd.config;

import lombok.Data;

/**
 * Application security configuration
 */
@Data
public class ApplicationSecurity {
    /**
     * JWT security configuration
     */
    private ApplicationSecurity.JWTConfiguration jwt;

    /**
     * Whether or not to enable security features
     */
    private boolean enabled;

    @Data
    static public class JWTConfiguration {
        /**
         * Secret used for hmac signing
         */
        private String secret;
        /**
         * Token expiration time
         */
        private long tokenValidityInSeconds;
        /**
         * Token expiration time with remember me flag
         */
        private long tokenValidityInSecondsForRememberMe;
    }
}