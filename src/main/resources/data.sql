INSERT INTO user (`id`, `email`, `password_hash`) VALUES
-- NOTE: Default password encryption is Bcrypt(10)
(1, 'admin@admin.com', '$2y$10$Un9/7amUxRbNgPyYDwTjPeqSj5rLqrPipGrBuDH3MrV.DiqVv7arG'),
(2, 'user@user.com',  '$2y$10$A7misNhkk5b1bK.z2B9nPO3fm/SvZDIinGIh1VbWLh8kdQjMOgR56');

INSERT INTO authority (`name`) VALUES
('ROLE_ADMIN'),
('ROLE_USER');

INSERT INTO user_authority (user_id, authority_name) VALUES
(1, 'ROLE_ADMIN'),
(1, 'ROLE_USER'),
(2, 'ROLE_USER');
